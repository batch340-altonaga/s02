-- CREATE A DATABASE
CREATE DATABASE blog_db;

-- USE THE DATABASE
USE blog_db;

-- USERS TABLE
CREATE TABLE users (
    id INT AUTO_INCREMENT NOT NULL,
    email VARCHAR(30) NOT NULL,
    password VARCHAR(30) NOT NULL,
    datetime_created DATETIME NOT NULL,
    PRIMARY KEY (id)
);

-- POSTS TABLE
CREATE TABLE posts (
    id INT AUTO_INCREMENT NOT NULL,
    user_id INT NOT NULL,
    title VARCHAR(500) NOT NULL,
    content VARCHAR(5000) NOT NULL,
    datetime_posted DATETIME NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_posts_user_id FOREIGN KEY(user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE RESTRICT
);

-- POST LIKES TABLE
CREATE TABLE post_likes (
    id INT AUTO_INCREMENT NOT NULL,
    user_id INT NOT NULL,
    post_id INT NOT NULL,
    datetime_liked DATETIME NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_posts_like_post_id FOREIGN KEY(post_id) REFERENCES posts(id) ON UPDATE CASCADE ON DELETE RESTRICT,
    CONSTRAINT fk_posts_like_user_id FOREIGN KEY(user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE RESTRICT
);

-- POST COMMENTS TABLE
CREATE TABLE post_comments (
    id INT AUTO_INCREMENT NOT NULL,
    post_id INT NOT NULL,
    user_id INT NOT NULL,
    content VARCHAR(5000) NOT NULL,
    datetime_commented DATETIME NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_posts_comments_like_post_id FOREIGN KEY(post_id) REFERENCES posts(id) ON UPDATE CASCADE ON DELETE RESTRICT,
    CONSTRAINT fk_posts_comments_like_user_id FOREIGN KEY(user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE RESTRICT
);